MySql is a relational database

MYSQL  is an open-source relational database management system (RDBMS).Its name is a combination of "My", 
the name of co-founder Michael Widenius's daughter,and "SQL", the abbreviation for Structured Query Language.

1. create a database,
    >  create database myDb

2. create table,
    >  CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255),
    Address varchar(255),
    City varchar(255)
);

3. insert a record,
    >  INSERT INTO `Persons`(`PersonID`, `LastName`, `FirstName`, `Address`, `City`) 
    VALUES (1,'anu','ani','jonggol','jawa');

4. select all data,
    >  SELECT * FROM `Persons`

5. select several atribut   
    >  SELECT `PersonID`, `LastName`, `FirstName` FROM `Persons`

6. delete existing record,
    >  DELETE FROM `persons` WHERE personID=1

7. 